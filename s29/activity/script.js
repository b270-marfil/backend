// Find users with letter s in their first name or d in their last name. Use the $or operator.

db.users.find(
    { $or: [ 
        {firstName : {$regex: 's', $options: "i"} }, 
        {lastName : {$regex: 'd', $options: "i"} }
    ]}, 
    {
        firstName: 1,
        lastName: 1,
        _id: 0
    }
);

// Find users who are from HR department and their age is greater than or equal to 70.

db.users.find({ $and: [ { department : "HR" }, { age : { $gte: 70 } } ]});

// Find users with the letter e in their first name and has an age of less than or equal to 30.

db.users.find(
    { $and: [ 
        {firstName : {$regex: 'e'} }, 
        {age : { $lte: 30 } }
    ]}
);
