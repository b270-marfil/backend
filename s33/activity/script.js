		fetch("https://jsonplaceholder.typicode.com/todos")

				.then(response => response.json())
				.then(lists => {
				console.dir(lists.map(lists => lists.title));
				});

		fetch("https://jsonplaceholder.typicode.com/todos/1")
				.then(response => response.json())
				.then(data => {
					const status = {
						completed: data.completed,
						data: data.id,
						title: data.title,
						userId: data.userId
					}
					console.log(status)
					const statusMessage = `The item "${status.title}" on the list has a status of ${status.completed}.`;
					console.log(statusMessage);
				});
		fetch("https://jsonplaceholder.typicode.com/todos", {
				method: "POST",
				headers: {
				"Content-Type" : "application/json"
				},
				body: JSON.stringify({
					completed: false,
					title: "Created To Do List Item",
					userId: 1
				})
			})
			.then(response => response.json())
			.then((data) => console.log(data));

			fetch("https://jsonplaceholder.typicode.com/todos/1", {
				method: "PUT",
				headers: {
					"Content-Type" : "application/json"
				},
				body: JSON.stringify({
					title: "Updated To Do List Item",
					description: "To update the my to do list with a different structure",
					status: "Pending",
					dateCompleted: "Pending",
					userId: 1
				})
			})
			.then(response => response.json())
			.then((data) => console.log(data));

			fetch("https://jsonplaceholder.typicode.com/todos/1", {
				method: "PATCH",
				headers: {
					"Content-Type" : "application/json"
				},
				body: JSON.stringify({
				status: "Complete",
				dateCompleted: "04/27/2023"
				})
			})

			.then(response => response.json())
			.then((data) => console.log(data));

			fetch("https://jsonplaceholder.typicode.com/todos/1", {
				method: "DELETE"
			})
			.then(response => response.json())
			.then(data => console.log(data));
