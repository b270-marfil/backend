// console.log("Happy Friday!")

// what are conditional statements?
// Conditional statements allow us to control the flow of our program. They also allow us to run statement/instructions if a constitution is met or run another separate instruction if otherwise.

// [SECTION] if, else if and else statement

let numA = -1;

// if statement - executes if a specified condition is true
/*
Syntax:
	if(condition) {
		statement
	}
*/
if(numA < 0){
	console.log("Hello!");
}

console.log(numA < 0);

let city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to New York City");
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo, Japan!");
}

// else if statement
// Execute a statement if previous conditions are false and if the specified condition is true

let numH = 1;

if(numH < 0) {
	console.log("Hello again!")
 } else if (numH > 0) {
 	console.log("World");
 }

 // else statement

 if (numA > 0){
 	console.log("Hello");
 } else if (numA === 0){
 	console.log("World")
 } else {
 	console.log("Again");
 }
// Will result to an error
 // else if {
 // 	console.log("Hi, B270!")
 // }

// if, else if and else statement with functions

function determineTyphoonIntensity(windSpeed){

	if(windSpeed < 30) {

		return "Not a typhoon yet";

	} else if (windSpeed <= 61) {

		return "Tropical depression detected.";

	} else if (windSpeed >= 62 && windSpeed <= 88) {

		return "Tropical storm detected."

	} else if (windSpeed >= 89 || windSpeed <= 117) {

		return "Severe tropical storm detected.";

	} else {

		return "Typhoon detected.";
	}
}

// Returns the string to the variable "message" that invoked it
let message = determineTyphoonIntensity(110);
console.log(message);

if(message == "Severe tropical storm detected.") {
	console.warn(message);
}

// [SECTION] Truthy and Falsy

if (true) {
	console.log("Truthy");
}

if (1) {
	console.log("Truthy");
}

if ([]) {
	console.log("Truthy");
}

// Falsy Examples
if(false) {
	console.log("Falsy");
}

if(0) {
	console.log("Falsy");
}

if (undefined) {
	console.log("Falsy");
}

// [SECTION] Conditional Ternary Operator
// Conditional Ternary operator takes in three operands
	// 1. condition
	// 2. expression to be executed if the condition is truthy
	// 3. expression to be executed if the condition is a falsy

/*
	Syntax:
	(expression) ? ifTrue : ifFalse;
*/
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

function isOfLegalAge() {
	name = "John";
	return "You are of legal age limit.";
}

function isUnderAge () {
	name = "Jane";
	return "You are under the age limit";
}

let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 17) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);

// [SECTION] Switch Statement

/*
	
	Syntax:
	switch (expression) {
	case value:
		statement;
		break;
	default: 
		statement;
		break;
	}

*/

// The .toLowerCase() will change the input received from the prompt into all lowercase ensuring a match with the switch cases if the user inputs capitalized or uppercased
let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
	case "monday":
	console.log("The color of the day is red.");
	break;

	case "tuesday":
	console.log("The color of the day is orange.");
	break;

	case "wednesday":
	console.log("The color of the day is yellow.");
	break;

	case "thursday":
	console.log("The color of the day is green.");
	break;

	case "friday":
	console.log("The color of the day is blue.");
	break;

	case "saturday":
	console.log("The color of the day is indigo.");
	break;

	case "sunday":
	console.log("The color of the day is violet.");
	break;

	default:
	console.log("Please input a valid day");
	break;
}

// [SECTION] Try-Catch-Finally

function showIntensityAlert(windSpeed){

	try {
		alerat(determineTyphoonIntensity(windSpeed))
	}
	catch (error) {
		console.log(typeof error)
		console.warn(error.message);
		console.warn(error.name);
	}
	finally {
		alert("Intensity updates will show new alert.")
	}
}
showIntensityAlert(56);