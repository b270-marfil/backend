// console.log("Hello World");

// Functions
	// Functions in JS are lines/blocks of codes that can tell our device/application to perform a certain task when called or invoked.

// Function Declarations
	// function statement - defines a function with a specified parameter

	/* Syntax:
		funcion functionName() {
			code block (statement)
		}
		- function keyword - used to define a JS function
		- functionName - is the function's name. Functions are named to be able to be used to called later in our code.
		- function block ({}) - the statements which comprise the body of the function. This is where the code to be executed is written.
	*/

	function printName() {
		console.log("Hi! My name is John.");

	};

	// Function invocation.
	printName();

	// declaredFunction(); - result to an error: Uncaught ReferenceError: declaredFunction is not defined

	// Semicolons are used to separate executable JS statements or codes.


// Function Declaration vs Function Expressions
	
	//1.) Function Declaration
		// declared functions can be hoisted. As long as the function has been declared.
	declaredFunction(); 

	function declaredFunction() {
		console.log("Hello World from declaredFunction()")
	};

	declaredFunction();

	//2.) Function Expression

		// A function can also be stored in a variable.
		// A function expression is anonymous function assigned to the variableFunction


		/*
			let variableName =  function() {
				code block(statement);
			}

		*/

		// This is how we initialize a variable
		// let n = 30;

		// variableFunction(); - this will result to an error: Uncaught ReferenceError: Cannot access 'variableFunction' before initialization

		let variableFunction = function() {
			console.log("Hello again Batch270!");		
		}

		variableFunction();

		// We invoke the function expression using its variable name, not its function name
		let funcExpression = function funcName () {
			console.log("Hello from the other side.")
		}

		funcExpression();

		// You can reassign declared functions and function expressions to a new anonymous functions.

		declaredFunction = function() {
			console.log("updated declaredFunction");
		}

		funcExpression = function() {
			console.log("updated funcExpression");
		}

		declaredFunction();
		funcExpression();

		const constantFunc = function() {
			console.log("Initialized with const");
		};

		constantFunc();
		declaredFunction();

/*		constantFunc = function() {
			console.log("Cannot be re-assigned");
		} - this will result to an error.
		constantFunc();*/

// Function Scoping

/*

	Scope is the accessibility (visibility) of variables within our program.

		Javascript Variables has 3 types of scope:
			1. local/block scope
			2. global scope
			3. function scope

*/

		// 1. Local Scope
		{

			let localVar = "Armando Perez";
			console.log(localVar);
		}
		// 2. Global Scope
		let globalVar = "Mr. World";

		console.log(globalVar);
		// console.log(localVar); - result to an error

		// 3. Function Scope

			function showNames() {
				// function scoped variables
				var functionVar = "Joe";
				const functionConst = "John";
				let functionLet = "Jane";

				console.log(functionVar);
				console.log(functionConst);
				console.log(functionLet);
			}
		
			showNames();

			// console.log(functionVar);
			// console.log(functionConst); - this will result into error
			// console.log(functionLet);

	// Nested Function
	// We can create another function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable name, as they are within the same scope/code block

	function myNewFunction() {
		let name = "Jane";

		function nestedFunction(){
			let nestedName = "John";
			console.log(name);
			console.log(nestedName);
		}
		nestedFunction();
	}
	// nestedFunction(); results to an error
	myNewFunction();

	// Function and Global Scoped Variables

	let globalName = "Alexandro";

	function myNewFunction2(){
		let nameInside = "Renz";
		console.log(globalName);
	}
	myNewFunction2();
	// console.log(nameInside); results to an error

// Using alert()
	
	// Syntax: alert("<messageInString>");
	// This will run immediately when the page loads
	alert("Hello World");

	function showSampleAlert() {
		alert("Hello, User!");
	}

	showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed.")

// Using prompt()

	let samplePrompt = prompt("Enter your Name.");
	console.log("Hello, " + samplePrompt);
	console.log(typeof samplePrompt);

	let sampleNullPrompt = prompt("Don't enter anything.");
	// console.log(sampleNullPrompt)
	// returns an empty string when there is no input or null if the user cancels the prompt

	function printWelcomeMessage() {

		let firstName = prompt("Enter your first name.");
		let lastName = prompt("Enter your last name.");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");
	}
	printWelcomeMessage();


// Function Naming Conventions
	// 1. Function names should be descriptive of the task that it will perform. It usually contains a verb

	function getCourses() {
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}
	getCourses();

	// 2. Avoid generic names to avoid confusion within your code.

	function get(){
		let name = "Jamie";
		console.log(name);
	}
	get();

	// 3. Avoid pointless and inappropriate function names

	function foo() {
		console.log(25%5);

	}
	foo();

	// 4. Name your functions using camelCasing

	function displayCarInfo() {
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	}
	displayCarInfo();