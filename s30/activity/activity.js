/*
	1. Use the count operator to count the total number of fruits on sale.
	2. Use the count operator to count the total number of fruits with stock more than or equal to  20.
	3. Use the average operator to get the average price of fruits onSale per supplier.
	4. Use the max operator to get the highest price of a fruit per supplier.
	5. Use the min operator to get the lowest price of a fruit per supplier.
*/

// 1. Use the count operator to count the total number of fruits on sale.

db.fruits.aggregate([
	{ $match: { onSale: true }},
	{ $group: 
		{
			_id: null,
			fruitsOnSale: { $sum: 1 }
		}
	},
	{
		$project: {
			_id: 0,
			fruitsOnSale: { $toInt: "$fruitsOnSale" }
		}
	}
]);

// 2. Use the count operator to count the total number of fruits with stock more than or equal to  20.

db.fruits.aggregate([
	{ $match: { stock: { $gte: 20 } }},
	{ $group: 
		{ 
			_id: null, 
			enoughStock: { $sum: 1 }
		}
	},
	{ 
		$project: { 
			_id: 0, 
			enoughStock: { $toInt: "$enoughStock" }
		}
	}
]);

// 3. Use the average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate([
	{ $match: { onSale: true }},
	{
		$group: {
			_id: "$supplier_id",
			average_price: { $avg: "$price" }
		}
	}
]);

// 4. Use the max operator to get the highest price of a fruit per supplier.

db.fruits.aggregate([
  	{ 
  		$group: { 
	  		_id: "$supplier_id",
		    max_price: { $max: "$price" }
    	}
    }
]);


// 5. Use the min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
	{ 
		$group: {
			_id: "$supplier_id",
			min_price: { $min: "$price" }
		}
	}
]);
