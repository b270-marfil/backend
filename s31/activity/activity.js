// 1. What directive is used by Node.js in loading the modules it needs?
	// 'require' directive


// 2. What Node.js module contains a method for server creation?
	// The Node.js built-in http module contains a method for creating a server.


// 3. What is the method of the http object responsible for creating a server using Node.js?
	// The method responsible for creating a server using Node.js is http.createServer().


// 4. What method of the response object allows us to set status codes and content types?
	// 'res.writeHead()' is used to set the HTTP response header that will be sent back to the client.


// 5. Where will console.log() output its contents when run in Node.js?
	// When 'console.log()' is run in Node.js, its contents will be output to the console or terminal window where the Node.js process was started.


// 6. What property of the request object contains the address's endpoint?
	// The property of the request object that contains the endpoint or URL path of the requested resource is 'req.url'.