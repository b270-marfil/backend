// console.log("Happy Thursday, Batch 270!");

// Function Parameters and Arguments

	// We learned that we can gather data from user input using prompt()

	function printInput(){
		let nickname = prompt("Enter your nickname:");
		console.log("Hi, " + nickname);
	}
	// printInput();

	// Consider this function:

	// We can directly pass data into the function.
	function printName(name) {
		console.log("My name is " + name);
	}
	printName("Juana");
	printName("John");
	printName("Jane");

	// "name" - parameter
	// A parameter acts a container/named variable that exists only inside a function

	// "Juana" - argument
	// An argument is the information/data provided directly into the function

	// Variables can also be passed as an argument
	let sampleVariable = "Yui";

	printName(sampleVariable);

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);

		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}
	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);


	// Function as Arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.")
	}

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	invokeFunction(argumentFunction);

	console.log(argumentFunction);


	// Multiple Parameters

	function createFullName(firstName, middleName, lastName) {
		console.log(firstName + " " + middleName + " " + lastName);
	}
	createFullName("Juan", "Dela", "Cruz");

	// In JS, providing more/less arguments than the expected parameters wll not return an error

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";
	createFullName(firstName, middleName, lastName);


// Return Statement

	function returnFullName(firstName, middleName, lastName) {

		
		return firstName + " " + middleName + " " + lastName;

		// Any line/block of code that comes after the return staement is ignored because it ends the function execution
		console.log("This message will not be printed.");
	}

	// Whatever value is returned from the returnFullName function is stored in the completeName variable
	let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
	console.log(completeName);

	// You can also create a variable inside the function to contain the result and return that variable instead.
	function returnAddress(city, country) {

		let fullAddress = city + "," + country;
		return fullAddress;
	}

	let myAddress = returnAddress("Cebu City", "Philippines");
	console.log(myAddress);
	

	function printPlayerInfo(username, level, job) {
		console.log("Username: " + username);
		console.log("Level: " + level);
		console.log("Job: " + job);
	}

	let user1 = printPlayerInfo("knight_white", 95, "Paladin");
	console.log(user1);