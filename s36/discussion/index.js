// Set up the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use al the routes defined in "taskRoute.js"
const taskRoutes = require("./routes/taskRoutes");

// Server setup
const app = express();
const port = 3001;

// Database connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.jxv3idp.mongodb.net/b270-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);
// Set notifications for database connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the cloud database"))

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Add the task route
// Allows all the task routes created in the "taskRoutes.js" file to use "/tasks"
app.use("/tasks", taskRoutes)

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));
