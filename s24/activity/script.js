// 1.
const getCube = 2 ** 3
console.log(`The cube of 2 is ${getCube}`);

// 2.
const fullAddress = ["258", "Washington Ave", "NW", "California", "90011"];
const [houseNumber, street, city, state, zipCode] = fullAddress

console.log(`I live at ${houseNumber} ${street} ${city} ${state} ${zipCode}`);

// 3.
let animal = {
	givenName: "Lolong",
	animalType: "saltwater crocodile",
	animalWeight: "1075 kgs",
	animalMeasurement: "20 ft 3 in."
}

let {givenName, animalType, animalWeight, animalMeasurement} = animal;

console.log(`${givenName} was a ${animalType}. He weighed at ${animalWeight} with a measurement of ${animalMeasurement}`);


// 4.
const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(number));

// 5.
const reduceNumber = numbers.reduce((accumulator, currentValue) => {
	return accumulator + currentValue;
});

console.log(reduceNumber);


// 6.
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
console.log(myDog);


