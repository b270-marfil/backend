// Single Room

db.users.insertOne({
	roomName: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	roomsAvailable: 10,
	isAvailable: false
})

// Multiple Rooms

db.users.insertMany([

	{
		roomName: "double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		roomsAvailable: 5,
		isAvailable: false
	},
	{
		roomName: "queen",
		accommodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		roomsAvailable: 15,
		isAvailable: false
	}
]);

// Finding a room
db.users.find({roomName: "double"});

// Updating a room
db.users.updateOne(
{
	roomName: "queen",
},
{
	$set: {
		roomsAvailable: 0,
	},
}
);

// Delete rooms

db.users.deleteMany({roomsAvailable: 0});